const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.listen(port);
console.log("API escuchando en el puerto" + port);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const cardController = require('./controllers/CardController');
const movementController = require('./controllers/MovementController');

app.post('/psnglobal/v1/login/',authController.logInV1);
app.post('/psnglobal/v1/logout/:id',authController.logOutV1);

app.get('/psnglobal/v1/users/',userController.getUserV1);
app.get('/psnglobal/v1/users/:id',userController.getUserByIdV1);
app.post('/psnglobal/v1/users',userController.createUserV1);
app.put('/psnglobal/v1/users/:id',userController.putUserByIdV1);
app.put('/psnglobal/v1/users/:id/password',userController.putUserPswV1);

app.get('/psnglobal/v1/accounts/',accountController.getAccountV1);
app.get('/psnglobal/v1/accounts/:id',accountController.getAccountByIdV1);
app.get('/psnglobal/v1/users/:id/accounts',userController.getUserAccountsV1);
app.get('/psnglobal/v1/accounts/:id/movements/:ptype',accountController.getAccountMovementsByIdV1);
app.get('/psnglobal/v1/accounts/:id/movements/',accountController.getAccountMovementsV1);
app.post('/psnglobal/v1/accounts/',accountController.createAccountV1);

app.get('/psnglobal/v1/cards/',cardController.getCardV1);
app.get('/psnglobal/v1/cards/:id',cardController.getCardByIdV1);
app.get('/psnglobal/v1/users/:id/cards/:type',userController.getUserCardsV1);
app.get('/psnglobal/v1/cards/:id/movements/:ptype',cardController.getCardMovementsV1);
app.post('/psnglobal/v1/cards/',cardController.createCardV1);

app.post('/psnglobal/v1/movements/',movementController.createMovementV1);
