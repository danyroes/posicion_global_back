const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/psnglobaldb/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";

function getUserV1(req,res){
  console.log("GET /psnglobal/v1/users/");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Consultando cliente');
  httpClient.get('user?'+mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "mensaje" : "Error obteniendo usuarios"
      }
      res.status(500);
      res.send(response);
    }
  );
}


function getUserByIdV1(req,res){
  console.log("GET /psnglobal/v1/users/:id");

  var id = req.params.id;
  var query = 'q={"user_id":' + id +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Consultando Cliente');
  httpClient.get('user?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "mensaje" : "Error obteniendo usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {
            "mensaje" : "Usuario no encontrado"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function createUserV1(req, res) {
  console.log("POST /psnglobal/v1/users");
  console.log("First_name is: " + req.body.first_name);
  console.log("Last_name is: " + req.body.last_name);
  console.log("Fec_nac: " + req.body.fec_nac);
  console.log("Gender: " + req.body.gender);
  console.log("Email is: " + req.body.email);
  console.log("Telephone: " + req.body.telephone);
  console.log("Street Address: " + req.body.street_address);
  console.log("Street Number: " + req.body.street_number);
  console.log("Postal Code: " + req.body.postal_code);
  console.log("City: " + req.body.city);
  console.log("Country: " + req.body.country);
  console.log("Avatar: " + req.body.avatar);

 var query = 's={"user_id" : -1}&l=1';
 console.log("la query GET es " + "user?" + query + "&" + mLabAPIKey);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     if(err){
       var response = {
         "mensaje" : "Error en el Alta del Usuario"
       }
       res.status(500);
       res.send(response);
     }

     if (body.length == 0 || body.length == undefined) {
       var response = {
         "mensaje" : "Error en el Alta del Usuario"
       }
       res.status(409);
       res.send(response);
     } else {
       console.log("Último usuario encontrado: " + body[0].user_id);
       var newUser = {
         "user_id" : body[0].user_id + 1,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "fec_nac" : req.body.fec_nac,
         "gender" : req.body.gender,
         "email" : req.body.email,
         "password" : crypt.hash(req.body.password),
         "telephone" : req.body.telephone,
         "street_address" : req.body.street_address,
         "street_number" : req.body.street_number,
         "postal_code" : req.body.postal_code,
         "city" : req.body.city,
         "country" : req.body.country,
         "avatar" : req.body.avatar
       };
       console.log('---Creando Usuario---');
       httpClient.post('user?'+mLabAPIKey,newUser,
         function(errPOST, resMLabPOST, bodyPOST){
           if(errPOST){
             var response = {
               "mensaje" : "Error en el Alta del Usuario"
             }
             res.status(500);
             res.send(response);
           }

           console.log("Usuario creado con éxito");
           var response = {
             "mensaje" : "Usuario creado con éxito",
             "idUsuario" : bodyPOST.user_id
           }
           res.status(200);
           res.send(response);
         }
       )
     }
   }
 );
}

function putUserByIdV1(req,res){
  console.log("PUT /psnglobal/v1/users/:id");
  console.log("First_name is: " + req.body.first_name);
  console.log("Last_name is: " + req.body.last_name);
  console.log("Fec_nac: " + req.body.fec_nac);
  console.log("Gender: " + req.body.gender);
  console.log("Email is: " + req.body.email);
  console.log("Telephone: " + req.body.telephone);
  console.log("Street Address: " + req.body.street_address);
  console.log("Street Number: " + req.body.street_number);
  console.log("Postal Code: " + req.body.postal_code);
  console.log("City: " + req.body.city);
  console.log("Country: " + req.body.country);
  console.log("Avatar: " + req.body.avatar);

  var query = 'q={"user_id": ' + req.params.id + '}';
  console.log("la query GET es " + query);

  var flagDelete = false;

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length == 0) {
        if (JSON.stringify(req.body) == '{}'){
          var response = {
            "mensaje" : "No ha sido posible dar de baja al usuario. Usuario Inexistente"
          }
        }else{
          var response = {
            "mensaje" : "No ha sido posible actualizar los datos del usuario. Usuario Inexistente"
          }
        }
        res.status(409);
        res.send(response);
      } else {
        query = 'q={"user_id" : ' + body[0].user_id +'}';
        console.log("la query PUT es: " + query);
        console.log("req.body: " + JSON.stringify(req.body));

        if (JSON.stringify(req.body) == '{}'){
          var putUser = {};
          putUser.user_id = body[0].user_id;
          putUser.status = "Usuario dado de baja";
          flagDelete = true;
        }else{
          var putUser = body[0];

          if(req.body.first_name != undefined){
            putUser["first_name"]=req.body.first_name;
            console.log("First_name is: " + req.body.first_name);
          }

          if(req.body.last_name != undefined){
            putUser["last_name"]=req.body.last_name;
            console.log("Last_name is: " + req.body.last_name);
          }

          if(req.body.fec_nac != undefined){
            putUser["fec_nac"]=req.body.fec_nac;
            console.log("Fec_nac: " + req.body.fec_nac);
          }

          if(req.body.gender != undefined){
            putUser["gender"]=req.body.gender;
            console.log("Gender: " + req.body.gender);
          }

          if(req.body.email != undefined){
            putUser["email"]=req.body.email;
            console.log("Email is: " + req.body.email);
          }

          if(req.body.password != undefined){
            putUser["password"]=crypt.hash(req.body.password);
            console.log("password: " + crypt.hash(req.body.password));
          }

          if(req.body.telephone != undefined){
            putUser["telephone"]=req.body.telephone;
            console.log("Telephone: " + req.body.telephone);
          }

          if(req.body.street_address != undefined){
            putUser["street_address"]=req.body.street_address;
            console.log("Street Address: " + req.body.street_address);
          }

          if(req.body.street_number != undefined){
            putUser["street_number"]=req.body.street_number;
            console.log("Street Number: " + req.body.street_number);
          }

          if(req.body.postal_code != undefined){
            putUser["postal_code"]=req.body.postal_code;
            console.log("Postal Code: " + req.body.postal_code);
          }

          if(req.body.city != undefined){
            putUser["city"]=req.body.city;
            console.log("City: " + req.body.city);
          }

          if(req.body.country != undefined){
            putUser["country"]=req.body.country;
            console.log("Country: " + req.body.country);
          }

          if(req.body.avatar != undefined){
            putUser["avatar"]=req.body.avatar;
            console.log("Avatar: " + req.body.avatar);
          }
        }

        httpClient.put("user?" + query + "&" + mLabAPIKey,putUser,
          function(errPUT, resMLabPUT, bodyPUT) {
            if (errPUT){
              if (flagDelete){
                var response = {
                  "mensaje" : "No ha sido posible dar de baja al usuario"
                }
              }else{
                var response = {
                  "mensaje" : "No ha sido posible actualizar los datos del usuario"
                }
              }
              res.status(500);
              res.send(response);
            }

            if (flagDelete){
              console.log("DELETE done");
              var response = {
                "mensaje" : "Usuario dado de baja correctamente",
              }
            }else{
              console.log("PUT done");
              var response = {
                "mensaje" : "Usuario actualizado correctamente",
                "userid" : body[0].user_id,
                "first_name" : body[0].first_name,
                "last_name" : body[0].last_name
              }
            }
            res.status(200);
            res.send(response);
          }
        )
      }
    }
  );
 }

 function putUserPswV1(req,res){
   console.log("PUT /psnglobal/v1/users/:id/password");
   console.log("password is: " + req.body.password);
   console.log("new_password is: " + req.body.new_password);

   var query = 'q={"user_id": ' + req.params.id + '}';
   console.log("la query GET es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {

       if (err){
         response = {
           "mensaje" : "Error realizando cambio de contraseña"
         }
         console.log("Error realizando cambio de contraseña");
         res.status(500);
         res.send(response);
       }

       if (body.length == 0) {
         var response = {
             "mensaje" : "No ha sido posible actualizar los datos del usuario. Usuario inexistente"
         }
         res.status(409);
         res.send(response);
       }else{

         var isPasswordcorrect = crypt.checkPassword(req.body.password,body[0].password);
         console.log("isPasswordcorrect " + isPasswordcorrect);

         if (!isPasswordcorrect){
           var response = {
               "mensaje" : "No ha sido posible actualizar los datos del usuario. Password incorrecto"
           }
           res.status(409);
           res.send(response);
         } else{

           query = 'q={"user_id" : ' + body[0].user_id +'}';
           console.log("la query PUT es: " + query);
           console.log("req.body: " + JSON.stringify(req.body));

           var putUser = body[0];

           putUser["password"]=crypt.hash(req.body.new_password);
           console.log("password is: " + putUser["password"]);

           httpClient.put("user?" + query + "&" + mLabAPIKey,putUser,
             function(errPUT, resMLabPUT, bodyPUT) {
               console.log("PUT done");
               if (errPUT){
                 response = {
                   "mensaje" : "Error realizando cambio de contraseña"
                 }
                 console.log("Error realizando cambio de contraseña");
                 res.status(500);
                 res.send(response);
               }else{

                 var response = {
                   "mensaje" : "Usuario actualizado correctamente",
                   "idUsuario" : body[0].user_id
                 }
                 res.status(200);
                 res.send(response);
               }
             }
           )
         }
       }
     }
   );
  }

function getUserAccountsV1(req,res){
  console.log("GET /psnglobal/v1/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"user_id":' + id +'}';
      query = query + '&s={"account_id" : -1}';

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get('account?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {
          "mensaje" : "Error obteniendo cuentas del usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log("Cuentas OK");
          var response = body;

        }else{
          var response = {
            "mensaje" : "Cuenta no encontrada"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function getUserCardsV1(req,res){
  console.log("GET /psnglobal/v1/users/:id/cards/:type");

  var id = req.params.id;
  var type = req.params.type;
  var query = 'q={"user_id":' + id + ',';
      query = query + '"card_type":' + type +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get('card?'+ query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {
          "mensaje" : "Error obteniendo tarjetas del usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log("Tarjetas OK");
          var response = body;

        }else{
          var response = {
            "mensaje" : "Tarjeta no encontrada"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

module.exports.getUserV1 = getUserV1;
module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.createUserV1 = createUserV1;
module.exports.putUserByIdV1 = putUserByIdV1;
module.exports.putUserPswV1 = putUserPswV1;
module.exports.getUserAccountsV1 = getUserAccountsV1;
module.exports.getUserCardsV1 = getUserCardsV1;
