const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/psnglobaldb/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";


function getAccountV1(req,res){
  console.log("GET /psnglobal/v1/accounts/");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Obteniendo las cuentas');
  httpClient.get('account?'+mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "mensaje" : "Error obteniendo cuentas"
      }
      res.status(500);
      res.send(response);
    }
  );
}

function getAccountByIdV1(req,res){
  console.log("GET /psnglobal/v1/accounts/:id");

  var id = req.params.id;
  var query = 'q={"account_id":' + id +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('account?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "mensaje" : "Error obteniendo cuentas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log('Cuenta encontrada');
          var response = body[0];
          res.status(200);
        }else{
          var response = {
            "mensaje" : "Cuenta no encontrada"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function getAccountMovementsV1(req,res){
  console.log("GET /psnglobal/v1/accounts/:id/movements/");

  var id = req.params.id;
  var query = 'q={"product_id":' + id +'}';
      query = query + '&s={"movement_id" : -1}';

  console.log("la query GET es: " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get('movement?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "mensaje" : "Error obteniendo Movimientos de Cuenta"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log("Movimientos de Cuenta encontrados");
          var response = body;
          res.status(200);
        }else{
          var response = {
            "mensaje" : "Movimientos de Cuenta no encontrados"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function getAccountMovementsByIdV1(req,res){
  console.log("GET /psnglobal/v1/accounts/:id/movements/:ptype");

  var id = req.params.id;
  var ptype = req.params.ptype;
  var query = 'q={"product_id":' + id +',';
      query = query + '"product_type":' + ptype +'}';
      query = query + '&s={"movement_id" : -1}';

  console.log("la query GET es: " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get('movement?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "mensaje" : "Error obteniendo Movimientos de Cuenta"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log("Movimientos de Cuenta encontrados");
          var response = body;
          res.status(200);
        }else{
          var response = {
            "mensaje" : "Movimientos de Cuenta no encontrados"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function createAccountV1(req, res) {
  console.log("POST /psnglobal/v1/accounts");
  console.log("iban is: " + req.body.iban);
  console.log("user_id is: " + req.body.user_id);
  console.log("balance is: " + req.body.balance);

 var query = 's={"account_id" : -1}&l=1';
 console.log("la query GET es " + "account?" + query + "&" + mLabAPIKey);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("account?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0 || body.length == undefined) {
       var response = {
         "mensaje" : "Error en el Alta de la Cuenta"
       }
       res.send(response);
     } else {
       console.log("Última cuenta encontrada: " + body[0].account_id);
       var newAccount = {
         "account_id" : body[0].account_id + 1,
         "iban" : req.body.iban,
         "user_id" : req.body.user_id,
         "balance" : req.body.balance
       };
       console.log('---Creando Cuenta---');
       httpClient.post('account?'+mLabAPIKey,newAccount,
         function(errPOST, resMLabPOST, bodyPOST){
           console.log("Cuenta creada con éxito");
           var response = {
             "mensaje" : "Cuenta creada con éxito",
             "idAccount" : bodyPOST.account_id,
             "iban" : bodyPOST.iban,
             "user_id" : bodyPOST.user_id,
             "balance" : bodyPOST.balance
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.getAccountV1 = getAccountV1;
module.exports.getAccountByIdV1 = getAccountByIdV1;
module.exports.getAccountMovementsV1 = getAccountMovementsV1;
module.exports.getAccountMovementsByIdV1 = getAccountMovementsByIdV1;
module.exports.createAccountV1 = createAccountV1;
