const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/psnglobaldb/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";

function logInV1(req,res){
  console.log("POST /psnglobal/v1/login");
  console.log("Email is: " + req.body.email);
  console.log("Password is: " + req.body.password);

  var email = req.body.email;
  var password = req.body.password;

  if (email == '' || password == '') {
    var response = {
      "mensaje" : "Error en Login: Email y/o Password no informados."
    }
    console.log("Error en Login: Email y/o Password no informados.");
    res.status(400);
    res.send(response);
  }else{

    var query = 'q={"email":"' + email +'"}';
    console.log("La query GET es " + query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log('Consultando Usuario');
    httpClient.get('user?'+ query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err){
          var response = {
            "mensaje" : "Error realizando logIn"
          }
          console.log("Error realizando logIn");
          res.status(500);
          res.send(response);
        }else if (body.length == 0) {
            var response = {
              "mensaje" : "Error en Login: Email incorrecto"
              }
              console.log("Error en Login: Email incorrecto");
            res.status(409);
            res.send(response);
          }else{
            var isPasswordcorrect = crypt.checkPassword(password,body[0].password);
            console.log("isPasswordcorrect " + isPasswordcorrect);
            if(!isPasswordcorrect){
              var response = {
                "mensaje" : "Error en Login: Passsword incorrecto"
                }
              console.log("Error en Login: Passsword incorrecto");
              res.status(409);
              res.send(response);
            }else{
              query = 'q={"user_id" : ' + body[0].user_id +'}';
              console.log("Query for put is " + query);
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT) {
                  if (errPUT){
                    var response = {
                      "mensaje" : "Error realizando logIn"
                    }
                    res.status(500);
                    res.send(response);
                  }else{
                    console.log("PUT done");
                    console.log("user_id " + body[0].user_id);
                    var response = {
                      "mensaje" : "Usuario logado con éxito",
                      "userid" : body[0].user_id,
                      "first_name" : body[0].first_name,
                      "last_name" : body[0].last_name
                    }
                    res.status(200);
                    res.send(response);
                  }
                }
              );
            }
          }
      }
    );
  }
}


function logOutV1(req, res) {
 console.log("POST /psnglobal/v1/logout/:id");

 var query = 'q={"user_id": ' + req.params.id + '}';
 console.log("la query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     if (err){
       response = {
         "mensaje" : "Error realizando logOut"
       }
       console.log("Error realizando logOut");
       res.status(500);
       res.send(response);
     }else if (body.length == 0) {
       var response = {
         "mensaje" : "Error en LogOut: Usuario no encontrado"
       }
       res.status(409);
       res.send(response);
     } else {
       query = 'q={"user_id" : ' + body[0].user_id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           if (errPUT){
             response = {
               "mensaje" : "Error realizando logIn"
             }
             res.status(500);
             res.send(response);
           }else{
             console.log("PUT done");
             var response = {
               "mensaje" : "Usuario deslogado con éxito",
               "userid" : body[0].user_id
             }
             res.status(200);
             res.send(response);
           }
         }
       )
     }
   }
 );
}


module.exports.logInV1 = logInV1;
module.exports.logOutV1 = logOutV1;
