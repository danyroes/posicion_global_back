const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/psnglobaldb/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";

function createMovementV1(req, res) {
  console.log("POST /psnglobal/v1/movements");
  console.log("Product_id is: " + req.body.product_id);
  console.log("Date is " + req.body.date);
  console.log("Sender is " + req.body.sender);
  console.log("Amount is: " + req.body.amount);
  console.log("Observations is: " + req.body.observations);
  console.log("Product_type is: " + req.body.product_type);
  console.log("Operator is: " + req.body.operator);
  console.log("Account_id is: " + req.body.account_id);

 var query = 's={"movement_id" : -1}&l=1';
 console.log("la query GET es " + "movement?" + query + "&" + mLabAPIKey);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("movement?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if(err){
       var response = {
         "mensaje" : "Error en el Alta del Movimiento"
       }
       res.status(500);
       res.send(response);
     }

     if (body.length == 0 || body.length == undefined) {
       var response = {
         "mensaje" : "Error en el Alta del Movimiento"
       }
       res.send(response);
       res.status(409);
     } else {
       console.log("Último movimiento encontrado: " + body[0].movement_id);

       var amount = req.body.amount
       if(req.body.operator == "-"){
         amount = req.body.operator + amount;
       }
       var newMovement = {
         "movement_id" : body[0].movement_id + 1,
         "product_id" : req.body.product_id,
         "date" : req.body.date,
         "sender" : req.body.sender,
         "amount" : amount,
         "observations" : req.body.observations,
         "product_type" : req.body.product_type
       };
       console.log('---Creando Movimiento---');
       httpClient.post('movement?'+mLabAPIKey,newMovement,
         function(errPOST, resMLabPOST, bodyPOST){
           if(errPOST){
             var response = {
               "mensaje" : "Error en el Alta del Movimiento"
             }
             res.send(response);
             res.status(500);
           }else{
             /*
             console.log("Movimiento creado con éxito");
             var response = {
               "mensaje" : "Movimiento creado con éxito",
               "idMovement" : bodyPOST.movement_id
             }
             res.status(200);
             res.send(response);
             */
             if(req.body.product_type == 1){
               var query = 'q={"account_id":' + req.body.account_id +'}';
               console.log("La query GET es " + query);
               var httpClient = requestJson.createClient(baseMLabURL);
               console.log('Consultando Cuenta');
               httpClient.get('account?'+ query + "&" + mLabAPIKey,
                 function(errUpd, resMLabUpd, bodyUpd){
                   if (errUpd){
                     var response = {
                       "mensaje" : "Movimiento Creado. Error consultado la Cuenta"
                     }
                     console.log("Movimiento Creado. Error consultado la Cuenta");
                     res.status(500);
                     res.send(response);
                   }else if (bodyUpd.length == 0) {
                       var response = {
                         "mensaje" : "Movimiento Creado. Cuenta no encontrada. No se puede actualizar"
                         }
                         console.log("Movimiento Creado. Cuenta no encontrada. No se puede actualizar");
                       res.status(409);
                       res.send(response);
                     }else{
                       console.log("JSON.stringify(bodyUpd[0]): " +JSON.stringify(bodyUpd[0]));
                       console.log("Actualizando Cuenta");
                       if(req.body.operator == '+'){
                         console.log("bodyUpd.balance: " +bodyUpd[0].balance);
                         console.log("req.body.amount: " +req.body.amount);
                         var newBalance = parseFloat(bodyUpd[0].balance) + parseFloat(req.body.amount);
                         newBalance = Number(newBalance.toFixed(2));
                       }else{
                         var newBalance = parseFloat(bodyUpd[0].balance) - parseFloat(req.body.amount);
                         newBalance = Number(newBalance.toFixed(2));
                       }
                       query = 'q={"account_id" : ' + bodyUpd[0].account_id +'}';
                       console.log("Query for put is " + query);
                       var putBody = '{"$set":{"balance":' + newBalance +'}}';
                       console.log(putBody);
                       httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                         function(errUpdPUT, resMLabAccPUT, bodyUpdPUT) {
                           if (errUpdPUT){
                             var response = {
                               "mensaje" : "Movimiento Creado. Error actualizando Cuenta"
                             }
                             res.status(500);
                             res.send(response);
                           }else{
                             console.log("PUT Cuenta done");
                             var response = {
                               "mensaje" : "Movimiento creado y Cuenta actualizada correctamente",
                             }
                             res.status(200);
                             res.send(response);
                           }
                         }
                       );
                     }
                 }
               );
            }else if(req.body.product_type == 2 || req.body.product_type == 3){
              var query = 'q={"account_id":' + req.body.account_id +'}';
              console.log("La query GET es " + query);
              var httpClient = requestJson.createClient(baseMLabURL);
              console.log('Consultando Cuenta');
              httpClient.get('account?'+ query + "&" + mLabAPIKey,
                function(errUpd, resMLabUpd, bodyUpd){
                  if (errUpd){
                    var response = {
                      "mensaje" : "Movimiento Creado. Error consultado la Cuenta"
                    }
                    console.log("Movimiento Creado. Error consultado la Cuenta");
                    res.status(500);
                    res.send(response);
                  }else if (bodyUpd.length == 0) {
                    var response = {
                      "mensaje" : "Movimiento Creado. Cuenta no encontrada. No se puede actualizar"
                    }
                    console.log("Movimiento Creado. Cuenta no encontrada. No se puede actualizar");
                    res.status(409);
                    res.send(response);
                  }else{
                    console.log("JSON.stringify(bodyUpd[0]): " +JSON.stringify(bodyUpd[0]));
                    console.log("Actualizando Cuenta");
                    if(req.body.operator == '+'){
                      console.log("bodyUpd.balance: " +bodyUpd[0].balance);
                      console.log("req.body.amount: " +req.body.amount);
                      var newBalance = parseFloat(bodyUpd[0].balance) + parseFloat(req.body.amount);
                      newBalance = Number(newBalance.toFixed(2));
                    }else{
                      var newBalance = parseFloat(bodyUpd[0].balance) - parseFloat(req.body.amount);
                      newBalance = Number(newBalance.toFixed(2));
                    }
                    query = 'q={"account_id" : ' + bodyUpd[0].account_id +'}';
                    console.log("Query for put is " + query);
                    var putBody = '{"$set":{"balance":' + newBalance +'}}';
                    console.log(putBody);
                    httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                      function(errUpdPUT, resMLabAccPUT, bodyUpdPUT) {
                        if (errUpdPUT){
                          var response = {
                            "mensaje" : "Movimiento Creado. Error actualizando Cuenta"
                          }
                          res.status(500);
                          res.send(response);
                        }else{
                          console.log("PUT Cuenta done");
                          var response = {
                            "mensaje" : "Movimiento creado y Cuenta actualizada correctamente",
                          }
                          res.status(200);
                          res.send(response);
                          }
                        }
                      );
                    }
                  }
                );
              }
           }
         }
       )
     }
   }
 );
}

module.exports.createMovementV1 = createMovementV1;
