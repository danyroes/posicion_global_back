const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/psnglobaldb/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";


function getCardV1(req,res){
  console.log("GET /psnglobal/v1/cards/");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Obteniendo las Tarjetas');
  httpClient.get('card?'+mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "mensaje" : "Error obteniendo Tarjetas"
      }
      res.status(500);
      res.send(response);
    }
  );
}

function getCardByIdV1(req,res){
  console.log("GET /psnglobal/v1/cards/:id");

  var id = req.params.id;
  var query = 'q={"card_id":' + id +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('card?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "mensaje" : "Error obteniendo Tarjetas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log('Tarjeta encontrada');
          var response = body[0];
          res.status(200);
        }else{
          var response = {
            "mensaje" : "Tarjeta no encontrada"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function getCardMovementsV1(req,res){
  console.log("GET /psnglobal/v1/cards/:id/movements/:ptype");

  var id = req.params.id;
  var ptype = req.params.ptype;
  var query = 'q={"product_id":' + id +',';
      query = query + '"product_type":' + ptype +'}';
      query = query + '&s={"movement_id" : -1}';

  console.log("La query GET es: " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get('movement?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "mensaje" : "Error obteniendo Movimientos"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          console.log("Movimientos encontrados");
          var response = body;
          res.status(200);
        }else{
          var response = {
            "mensaje" : "Movimientos no encontrados"
          }
          res.status(409);
        }
      }
      res.send(response);
    }
  );
}

function createCardV1(req, res) {
  console.log("POST /psnglobal/v1/cards");
  console.log("PAN is: " + req.body.pan);
  console.log("card_type is: " + req.body.card_type);
  console.log("limit is: " + req.body.limit);
  console.log("user_id is: " + req.body.user_id);
  console.log("account_id is: " + req.body.account_id);

 var query = 's={"card_id" : -1}&l=1';
 console.log("la query GET es " + "card?" + query + "&" + mLabAPIKey);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("card?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     if(err){
       var response = {
         "mensaje" : "Error en el Alta de la Tarjeta"
       }
       res.status(500);
       res.send(response);
     }

     if (body.length == 0 || body.length == undefined) {
       var response = {
         "mensaje" : "Error en el Alta de la Tarjeta"
       }
       res.status(409);
       res.send(response);
     } else {
       console.log("Última tarjeta encontrada: " + body[0].card_id);
       var newAccount = {
         "card_id" : body[0].card_id + 1,
         "pan" : req.body.pan,
         "card_type" : req.body.card_type,
         "limit" : req.body.limit,
         "user_id" : req.body.user_id,
         "account_id" : req.body.account_id
       };
       console.log('---Creando Tarjeta---');
       httpClient.post('card?'+mLabAPIKey,newAccount,
         function(errPOST, resMLabPOST, bodyPOST){
           console.log("Tarjeta creada con éxito");
           var response = {
             "mensaje" : "Tarjeta creada con éxito",
             "idCard" : bodyPOST.card_id,
             "pan" : bodyPOST.pan,
             "card_type" : bodyPOST.card_type,
             "limit" : bodyPOST.limit,
             "user_id" : bodyPOST.user_id,
             "account_id" : bodyPOST.account_id
           }
           res.status(200);
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.getCardV1 = getCardV1;
module.exports.getCardByIdV1 = getCardByIdV1;
module.exports.getCardMovementsV1 = getCardMovementsV1;
module.exports.createCardV1 = createCardV1;
